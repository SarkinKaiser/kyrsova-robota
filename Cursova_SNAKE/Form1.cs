﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cursova_SNAKE
{
    public partial class Form1 : Form
    {
        private int rI, rJ;
        private PictureBox fruit;
        private PictureBox[] snake = new PictureBox[400];
        private Label Score;
        private int dirX, dirY;
        private int _width = 900;
        private int _hight = 800;
        private int _sizeofsides = 40;
        private int score = 0;
        public Form1()
        {
            InitializeComponent();
            this.Text = "Змія";
            this.Width = _width;
            this.Height = _hight;
            dirX = 1;
            dirY = 0;
            Score = new Label();
            Score.Text = "Очки: 0";
            Score.Location = new Point(810, 10);
            this.Controls.Add(Score);
            snake[0] = new PictureBox();
            snake[0].Location = new Point(201, 201);
            snake[0].Size = new Size(_sizeofsides - 1 , _sizeofsides - 1);
            snake[0].BackColor = Color.DarkGreen;
            this.Controls.Add(snake[0]);
            fruit = new PictureBox();
            fruit.BackColor = Color.Red;
            fruit.Size = new Size(_sizeofsides, _sizeofsides);
            GenMap();
            GenFruit();
            timer.Tick += new EventHandler(_update);
            timer.Interval = 300;
            timer.Start();
            this.KeyDown += new KeyEventHandler(KEI);
        }

        private void GenFruit()
        {
            Random r = new Random();
            rI = r.Next(0, _hight - _sizeofsides);
            int tempI = rI % _sizeofsides;
            rI -= tempI;
            rJ = r.Next(0, _hight - _sizeofsides);
            int tempJ = rJ % _sizeofsides;
            rJ -= tempJ;
            rI++;
            rJ++;
            fruit.Location = new Point(rI, rJ);
            this.Controls.Add(fruit);
        }

        private void Borders()
        {
            if (snake[0].Location.X < 0)
            {
                for(int _i = 1; _i <= score; _i++)
                {
                    this.Controls.Remove(snake[_i]);
                }
                timer.Stop();
                MessageBox.Show("Ваші очки: " + score);
                timer.Start();
                score = 0;
                Speed();
                Score.Text = "Очки: " + score;
                dirX = 1;
            }
            if (snake[0].Location.X > _hight)
            {
                for (int _i = 1; _i <= score; _i++)
                {
                    this.Controls.Remove(snake[_i]);
                }
                timer.Stop();
                MessageBox.Show("Ваші очки: " + score);
                timer.Start();
                score = 0;
                Speed();
                Score.Text = "Очки: " + score;
                dirX = -1;
            }
            if (snake[0].Location.Y < 0)
            {
                for (int _i = 1; _i <= score; _i++)
                {
                    this.Controls.Remove(snake[_i]);
                }
                timer.Stop();
                MessageBox.Show("Ваші очки: " + score);
                timer.Start();
                score = 0;
                Speed();
                Score.Text = "Очки: " + score;
                dirY = 1;
            }
            if (snake[0].Location.Y > _hight)
            {
                for (int _i = 1; _i <= score; _i++)
                {
                    this.Controls.Remove(snake[_i]);
                }
                timer.Stop();
                MessageBox.Show("Ваші очки: " + score);
                Speed();
                timer.Start();
                score = 0;
                Speed();
                Score.Text = "Очки: " + score;
                dirY = -1;
            }
        }

        private void EatSnake()
        {
            for (int _i = 1; _i < score; _i++)
            {
                if (snake[0].Location == snake[_i].Location)
                {
                    for (int _j = _i; _j <= score; _j++)
                        this.Controls.Remove(snake[_j]);
                    timer.Stop();
                    MessageBox.Show("Ти сам себе з'їв! Ваші очки: " + score);
                    score = score - (score - _i + 1);
                    MessageBox.Show("Ви можете продовжувати набирати очки: " + score + "\n Готовий? Розпочинай!!!");
                    timer.Start();
                    Speed();
                    Score.Text = "Очки: " + score;
                }
            }
        }

        private void EatFruit()
        {
            if (snake[0].Location.X == rI && snake[0].Location.Y == rJ)
            {
                Score.Text = "Очки: " + ++score;
                Speed();
                snake[score] = new PictureBox();
                snake[score].Location = new Point(snake[score - 1].Location.X + 40 * dirX, snake[score - 1].Location.Y - 40 * dirY);
                snake[score].Size = new Size(_sizeofsides - 1, _sizeofsides - 1);
                snake[score].BackColor = Color.DarkGreen;
                this.Controls.Add(snake[score]);
                GenFruit();
            }
        }

        private void GenMap()
        {
            for (int i = 0; i <_width / _sizeofsides; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.Black;
                pic.Location = new Point(0, _sizeofsides * i);
                pic.Size = new Size(_width - 100, 1);
                this.Controls.Add(pic);
            }
            for (int i = 0; i <= _hight / _sizeofsides; i++)
            {
                PictureBox pic = new PictureBox();
                pic.BackColor = Color.Black;
                pic.Location = new Point(_sizeofsides * i, 0);
                pic.Size = new Size(1, _width);
                this.Controls.Add(pic);
            }
        }

        private void Move()
        {
            for (int i = score; i >= 1; i--)
            {
                snake[i].Location = snake[i - 1].Location;
            }
            snake[0].Location = new Point(snake[0].Location.X + dirX * (_sizeofsides), snake[0].Location.Y + dirY * (_sizeofsides));
            EatSnake();
        }

        private void _update (Object myObject,EventArgs eventArgs)
        {
            Borders();
            EatFruit();
            Move();
        }

        private void Speed()
        {
            int speed = 300;
            if ((0 <= score) && (score < 5))
            {
                speed = speed - 50;
            }
            else
            {
                if ((5 <= score) && (score < 10))
                    speed = speed - 100;
                else
                {
                    if ((10 <= score) && (score < 15))
                        speed = speed - 125;
                    else
                    {
                        if ((15 <= score) && (score < 20))
                            speed = speed - 150;
                        else speed = speed - 200;
                    }
                }
            }
            timer.Interval = speed;
        }

        private void KEI(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode.ToString())
            {
                case "Right":
                    dirX = 1;
                    dirY = 0;
                    break;
                case "Left":
                    dirX = -1;
                    dirY = 0;
                    break;
                case "Up":
                    dirY = -1;
                    dirX = 0;
                    break;
                case "Down":
                    dirY = 1;
                    dirX = 0;
                    break;
                default:
                    timer.Stop();
                    MessageBox.Show("Пауза! Ваша кількість очок на даний момент: " + score + "\n Щоб продовжити, натисніть OK!");
                    timer.Start();
                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
